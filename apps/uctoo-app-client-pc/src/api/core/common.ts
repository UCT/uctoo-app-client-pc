import { requestClient } from '#/api/request';

// 图片上传
export const fileUpload = async (formData: FormData) => {
  return requestClient.post('/attachments/fileUpload', formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
};
