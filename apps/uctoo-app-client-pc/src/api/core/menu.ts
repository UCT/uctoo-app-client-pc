import type { RouteRecordStringComponent } from '@vben/types';

import { requestClient } from '#/api/request';
import { useAuthStore } from '#/store';
import { resetAllStores, useAccessStore, useUserStore } from '@vben/stores';

/**
 * 获取用户所有菜单
 */
export async function getAllMenusApi() {
  const res = await requestClient.post<RouteRecordStringComponent[]>('/permissions/menu/userMenuAll');
  if(res.length === 0){  //加载不到菜单退出回登录页
  console.log('getAllMenusApi.length0');
        const accessStore = useAccessStore();
        const authStore = useAuthStore();
        accessStore.setAccessToken(null);
        accessStore.setLoginExpired(true);
        await authStore.logout();
  }
  return res;
}
