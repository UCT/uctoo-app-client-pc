import type { RouteRecordRaw } from 'vue-router';

import { BasicLayout } from '#/layouts';

const routes: RouteRecordRaw[] = [
  {
    component: BasicLayout,
    meta: {
      icon: 'lucide:layout-dashboard',
      order: -1,
      title: '商品管理',
    },
    name: 'Shop',
    path: '/shop',
    children: [
      {
        name: 'SPU',
        path: '/spu',
        component: () => import('#/views/shop/spu/index.vue'),
        meta: {
          affixTab: true,
          icon: 'lucide:area-chart',
          title: '商品SPU管理',
        },
      },
    ],
  },
];

export default routes;
