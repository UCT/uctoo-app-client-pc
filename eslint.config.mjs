// import { vbenConfig } from '@vben/eslint-config';

export default [
  // ...vbenConfig,
  {
    rules: {
      'vue/html-self-closing': 'off',
      'vue/singleline-html-element-content-newline': 'off',
      'vue/html-closing-bracket-newline': 'off',
    },
  },
];
