export interface MinishopConfig {
[property: string]: any;
id:string;
name:string;
group:string;
title:string;
tip:string;
type:string;
value:string;
content:string;
rule:string;
extend:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopConfigs: MinishopConfig[];
  total: number;
}