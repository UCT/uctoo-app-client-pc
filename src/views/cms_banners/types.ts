export interface CmsBanners {
[property: string]: any;
id:string;
title:string;
banner_img:string;
category_id:string;
link_to:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  cmsBannerss: CmsBanners[];
  total: number;
}