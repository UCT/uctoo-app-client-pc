export interface Company {
[property: string]: any;
id:string;
company_name:string;
region:string;
social_credit_code:string;
established_time:string;
registered_capital:string;
registered_address:string;
mailing_address:string;
legal_representative:string;
legal_representativeMobile:string;
legal_representative_email:string;
contact_name:string;
contact_mobile:string;
contact_email:string;
contact_title:string;
website:string;
company_logo:string;
associated_project_manager:string;
verified:number;
company_introduction:string;
business_licence:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  companys: Company[];
  total: number;
}