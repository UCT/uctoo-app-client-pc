export interface Faq {
[property: string]: any;
id:string;
title:string;
content:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  faqs: Faq[];
  total: number;
}