export interface MinishopOrder {
[property: string]: any;
id:string;
order_id:string;
out_order_id:string;
out_trade_no:string;
openid:string;
user_id:string;
type:number;
path:string;
scene:number;
order_detail_id:string;
address_info_id:string;
out_aftersale_id:string;
ticket:string;
ticket_expire_time:Date;
final_price:number;
ext_json:string;
platform:string;
transaction_id:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
status:number;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopOrders: MinishopOrder[];
  total: number;
}