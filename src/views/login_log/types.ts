export interface LoginLog {
[property: string]: any;
id:string;
login_name:string;
login_ip:string;
browser:string;
os:string;
login_at:Date;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  loginLogs: LoginLog[];
  total: number;
}