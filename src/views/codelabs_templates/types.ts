export interface CodelabsTemplates {
[property: string]: any;
id:string;
name:string;
template_code:string;
filename:string;
filepath:string;
config_header:string;
config_json:string;
template_url:string;
opensource:number;
author:string;
author_url:string;
user_id:string;
tags:string;
docs:string;
images:string;
description:string;
price:string;
status:number;
arguments:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  codelabsTemplatess: CodelabsTemplates[];
  total: number;
}