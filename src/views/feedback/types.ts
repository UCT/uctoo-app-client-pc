export interface Feedback {
[property: string]: any;
id:string;
user_id:string;
type:string;
content:string;
images:string;
phone:string;
remark:string;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  feedbacks: Feedback[];
  total: number;
}