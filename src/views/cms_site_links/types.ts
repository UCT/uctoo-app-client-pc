export interface CmsSiteLinks {
[property: string]: any;
id:string;
title:string;
link_to:string;
weight:number;
is_show:number;
icon:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  cmsSiteLinkss: CmsSiteLinks[];
  total: number;
}