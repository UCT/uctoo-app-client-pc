export interface Crontab {
[property: string]: any;
id:string;
name:string;
group:string;
task:string;
cron:string;
tactics:string;
remark:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
status:number;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  crontabs: Crontab[];
  total: number;
}