export interface CustomerType {
[property: string]: any;
id:string;
name:string;
title:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  customerTypes: CustomerType[];
  total: number;
}