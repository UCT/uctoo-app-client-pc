export interface GroupTag {
[property: string]: any;
id:string;
group_id:string;
tagId:string;
created_at:Date;
creator:string;
deleted_at:Date;
updated_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  groupTags: GroupTag[];
  total: number;
}