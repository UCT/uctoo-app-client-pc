export interface GuestUsers {
[property: string]: any;
id:string;
nickname:string;
token:string;
domain:string;
ipAddress:string;
userAgent:string;
referer:string;
lastAccessTime:Date;
mobile:string;
channel:string;
comments:string;
user_id:string;
user_model:string;
appid:string;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  guestUserss: GuestUsers[];
  total: number;
}