export interface CrmCardHolder {
[property: string]: any;
id:string;
holder_uid:string;
holder_umodel:string;
card_uid:string;
card_umodel:string;
nanoid:string;
weight:number;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  crmCardHolders: CrmCardHolder[];
  total: number;
}