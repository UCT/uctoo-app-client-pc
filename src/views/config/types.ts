export interface Config {
[property: string]: any;
id:string;
name:string;
parent_id:string;
component:string;
key:string;
value:string;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  configs: Config[];
  total: number;
}