export interface MinishopGoodsService {
[property: string]: any;
id:string;
name:string;
image:string;
description:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopGoodsServices: MinishopGoodsService[];
  total: number;
}