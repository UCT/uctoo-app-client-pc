export interface MinishopDecorate {
[property: string]: any;
id:string;
name:string;
type:string;
image:string;
memo:string;
platform:string;
status:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopDecorates: MinishopDecorate[];
  total: number;
}