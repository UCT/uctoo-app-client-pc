export interface MinishopOrderAction {
[property: string]: any;
id:string;
order_id:string;
out_order_id:string;
order_item_id:string;
oper_type:string;
oper_id:string;
order_status:number;
dispatch_status:number;
comment_status:number;
aftersale_status:number;
refund_status:number;
remark:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopOrderActions: MinishopOrderAction[];
  total: number;
}