export interface MinishopActivityGroupon {
[property: string]: any;
id:string;
user_id:string;
goods_id:string;
activity_id:string;
num:number;
current_num:number;
expire_time:Date;
finish_time:Date;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
status:string;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopActivityGroupons: MinishopActivityGroupon[];
  total: number;
}