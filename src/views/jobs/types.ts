export interface Jobs {
[property: string]: any;
id:string;
job_name:string;
coding:string;
description:string;
weight:number;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  jobss: Jobs[];
  total: number;
}