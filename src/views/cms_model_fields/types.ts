export interface CmsModelFields {
[property: string]: any;
id:string;
title:string;
name:string;
type:string;
length:string;
default_value:string;
options:string;
is_index:number;
is_unique:number;
rules:string;
pattern:string;
model_id:string;
weight:number;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
used_at_detail:number;
used_at_search:number;
used_at_list:number;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  cmsModelFieldss: CmsModelFields[];
  total: number;
}