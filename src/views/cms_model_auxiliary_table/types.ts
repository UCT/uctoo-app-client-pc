export interface CmsModelAuxiliaryTable {
[property: string]: any;
id:string;
model_id:string;
alias:string;
table_name:string;
description:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
used:number;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  cmsModelAuxiliaryTables: CmsModelAuxiliaryTable[];
  total: number;
}