export interface ElementComponents {
[property: string]: any;
id:string;
name:string;
tag:string;
docs_url:string;
specification:string;
version:string;
remarks:string;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  elementComponentss: ElementComponents[];
  total: number;
}