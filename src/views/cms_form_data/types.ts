export interface CmsFormData {
[property: string]: any;
id:string;
form_id:string;
form_data:string;
user_id:string;
ip:string;
user_agent:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  cmsFormDatas: CmsFormData[];
  total: number;
}