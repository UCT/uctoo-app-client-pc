export interface MinishopActivityGoodsSkuPrice {
[property: string]: any;
id:string;
activity_id:string;
sku_price_id:string;
goods_id:string;
stock:number;
sales:number;
price:string;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopActivityGoodsSkuPrices: MinishopActivityGoodsSkuPrice[];
  total: number;
}