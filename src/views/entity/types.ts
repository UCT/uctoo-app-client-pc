export interface Entity {
[property: string]: any;
id:string;
link:string;
privacy_level:number;
stars:string;
description:string;
group_id:string;
picture:string;
images:string;
content:string;
json:string;
city:string;
price:string;
birthday:string;
owner:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  entitys: Entity[];
  total: number;
}