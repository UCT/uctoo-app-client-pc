export interface AiClient {
[property: string]: any;
id:number;
domain:string;
username:string;
password:string;
token:string;
token_overtime:Date;
created_at:Date;
updated_at:Date;
deleted_at:Date;
creator:string;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  aiClients: AiClient[];
  total: number;
}