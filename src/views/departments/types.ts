export interface Departments {
[property: string]: any;
id:string;
department_name:string;
parent_id:string;
principal:string;
mobile:string;
email:string;
weight:number;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  departmentss: Departments[];
  total: number;
}