export interface MinishopDecorateContent {
[property: string]: any;
id:string;
type:string;
category:string;
name:string;
content:string;
decorate_id:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
status:number;
weight:number;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopDecorateContents: MinishopDecorateContent[];
  total: number;
}