export interface DeveloperAccount {
[property: string]: any;
id:string;
type:number;
user_id:string;
wechat_miniapp_user_id:string;
realname:string;
mobile:string;
idcard1:string;
idcard2:string;
business_license:string;
wechat_openid:string;
mchid:string;
alipay_user_Id:string;
alipay_login_name:string;
company_name:string;
company_address:string;
company_contact:string;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  developerAccounts: DeveloperAccount[];
  total: number;
}