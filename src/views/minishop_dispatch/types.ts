export interface MinishopDispatch {
[property: string]: any;
id:string;
name:string;
type:string;
type_ids:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopDispatchs: MinishopDispatch[];
  total: number;
}