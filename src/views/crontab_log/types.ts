export interface CrontabLog {
[property: string]: any;
id:string;
crontab_id:string;
used_time:number;
error_message:string;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  crontabLogs: CrontabLog[];
  total: number;
}