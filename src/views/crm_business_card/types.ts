export interface CrmBusinessCard {
[property: string]: any;
id:string;
uid:string;
umodel:string;
realname:string;
gender:string;
cover:string;
job_title:string;
sub_title:string;
organization:string;
org_logo:string;
mobile:string;
wechat:string;
address:string;
email:string;
attrs:string;
nanoid:string;
phone:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  crmBusinessCards: CrmBusinessCard[];
  total: number;
}