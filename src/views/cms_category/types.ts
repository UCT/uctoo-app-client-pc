export interface CmsCategory {
[property: string]: any;
id:string;
name:string;
title:string;
keywords:string;
description:string;
url:string;
status:number;
is_can_contribute:number;
is_can_comment:number;
type:number;
weight:number;
link_to:string;
limit:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
parent_id:string;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  cmsCategorys: CmsCategory[];
  total: number;
}