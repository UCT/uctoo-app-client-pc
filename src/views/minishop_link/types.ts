export interface MinishopLink {
[property: string]: any;
id:string;
name:string;
path:string;
group:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopLinks: MinishopLink[];
  total: number;
}