export interface CodelabsAlgorithm {
[property: string]: any;
id:string;
command_name:string;
algorithm_code:string;
filename:string;
filepath:string;
template_url:string;
opensource:number;
author:string;
author_url:string;
user_id:string;
tags:string;
docs:string;
images:string;
description:string;
price:string;
status:number;
arguments:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  codelabsAlgorithms: CodelabsAlgorithm[];
  total: number;
}