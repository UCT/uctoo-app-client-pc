export interface MinishopArea {
[property: string]: any;
id:string;
name:string;
parent_id:string;
level:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopAreas: MinishopArea[];
  total: number;
}