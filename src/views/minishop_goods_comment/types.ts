export interface MinishopGoodsComment {
[property: string]: any;
id:string;
goods_id:string;
order_id:string;
order_item_id:string;
user_id:string;
level:number;
content:string;
images:string;
reply_content:string;
reply_time:Date;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopGoodsComments: MinishopGoodsComment[];
  total: number;
}