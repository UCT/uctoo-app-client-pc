export interface MinishopCart {
[property: string]: any;
id:string;
user_id:string;
minishop_spu_id:string;
minishop_sku_id:string;
product_id:string;
out_product_id:string;
sku_id:string;
out_sku_id:string;
product_cnt:number;
openid:string;
appid:string;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopCarts: MinishopCart[];
  total: number;
}