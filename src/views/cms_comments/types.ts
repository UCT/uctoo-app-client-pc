export interface CmsComments {
[property: string]: any;
id:string;
article_id:string;
content:string;
user_id:string;
ip:string;
user_agent:string;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
parent_id:string;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  cmsCommentss: CmsComments[];
  total: number;
}