export interface CmsForms {
[property: string]: any;
id:string;
name:string;
alias:string;
submit_url:string;
title:string;
keywords:string;
description:string;
success_message:string;
failed_message:string;
success_link_to:string;
is_login_to_submit:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  cmsFormss: CmsForms[];
  total: number;
}