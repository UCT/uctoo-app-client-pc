export interface Attachments {
[property: string]: any;
id:string;
path:string;
url:string;
mime_type:string;
file_ext:string;
filesize:number;
filename:string;
driver:string;
scene:string;
type:string;
sc_id:number;
ai_task_id:number;
created_at:Date;
updated_at:Date;
deleted_at:Date;
creator:string;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  attachmentss: Attachments[];
  total: number;
}