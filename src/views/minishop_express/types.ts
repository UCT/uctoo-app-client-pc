export interface MinishopExpress {
[property: string]: any;
id:string;
name:string;
code:string;
weigh:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopExpresss: MinishopExpress[];
  total: number;
}