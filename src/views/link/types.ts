export interface Link {
[property: string]: any;
id:string;
link:string;
privacy_cevel:number;
owner:string;
stars:string;
description:string;
group_id:string;
created_at:Date;
deleted_at:Date;
updated_at:Date;
creator:string;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  links: Link[];
  total: number;
}