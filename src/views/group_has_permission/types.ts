export interface GroupHasPermission {
[property: string]: any;
group_id:string;
permission_name:string;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  groupHasPermissions: GroupHasPermission[];
  total: number;
}