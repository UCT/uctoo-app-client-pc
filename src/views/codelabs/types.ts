export interface Codelabs {
[property: string]: any;
id:string;
command:string;
arguments:string;
data_structure:string;
template_code:string;
config_data:string;
algorithm:string;
result:string;
input:string;
output:string;
remark:string;
template_file:string;
data_source:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  codelabss: Codelabs[];
  total: number;
}