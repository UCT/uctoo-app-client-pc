export interface CmsFormFields {
[property: string]: any;
id:string;
form_id:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
default_value:string;
failed_message:string;
label:string;
length:number;
name:string;
rule:string;
status:number;
type:string;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  cmsFormFieldss: CmsFormFields[];
  total: number;
}