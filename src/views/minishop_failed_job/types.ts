export interface MinishopFailedJob {
[property: string]: any;
id:string;
data:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopFailedJobs: MinishopFailedJob[];
  total: number;
}