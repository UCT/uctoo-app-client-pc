export interface AppAccessToken {
[property: string]: any;
id:string;
access_token:string;
token_overtime:Date;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  appAccessTokens: AppAccessToken[];
  total: number;
}