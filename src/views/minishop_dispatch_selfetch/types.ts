export interface MinishopDispatchSelfetch {
[property: string]: any;
id:string;
store_ids:string;
expire_type:string;
expire_day:number;
expire_time:Date;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopDispatchSelfetchs: MinishopDispatchSelfetch[];
  total: number;
}