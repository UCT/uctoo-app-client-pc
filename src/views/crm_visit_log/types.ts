export interface CrmVisitLog {
[property: string]: any;
id:string;
from_uid:string;
from_umodel:string;
to_uid:string;
to_umodel:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  crmVisitLogs: CrmVisitLog[];
  total: number;
}