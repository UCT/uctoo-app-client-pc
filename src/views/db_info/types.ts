export interface DbInfo {
[property: string]: any;
id:string;
table_catalog:string;
table_schema:string;
table_name:string;
column_name:string;
column_default:string;
is_nullable:string;
data_type:string;
vue_component_type:string;
react_component_type:string;
arkui_component_type:string;
uniapp_component_type:string;
rules:string;
pattern:string;
weight:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
placeholder:string;
is_column_hidden:string;
is_table_hidden:string;
column_comment:string;
ordinal_position:number;
migration_id:string;
character_maximum_length:number;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  dbInfos: DbInfo[];
  total: number;
}