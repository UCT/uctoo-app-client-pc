export interface AdminApplet {
[property: string]: any;
appid:string;
status:number;
created_at:Date;
updated_at:Date;
deleted_at:Date;
creator:string;
id:string;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  adminApplets: AdminApplet[];
  total: number;
}