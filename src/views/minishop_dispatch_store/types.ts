export interface MinishopDispatchStore {
[property: string]: any;
id:string;
name:string;
store_ids:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopDispatchStores: MinishopDispatchStore[];
  total: number;
}