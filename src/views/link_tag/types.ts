export interface LinkTag {
[property: string]: any;
id:string;
link_id:string;
tag_id:string;
created_at:Date;
creator:string;
deleted_at:Date;
updated_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  linkTags: LinkTag[];
  total: number;
}