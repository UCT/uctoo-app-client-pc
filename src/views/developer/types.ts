export interface Developer {
[property: string]: any;
id:string;
user_id:string;
mobile:string;
id_card:string;
alipay_account:string;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  developers: Developer[];
  total: number;
}