export interface CmsArticles {
[property: string]: any;
id:string;
title:string;
cover:string;
images:string;
url:string;
content:string;
keywords:string;
description:string;
pv:number;
likes:number;
comment_num:number;
is_top:number;
is_recommend:number;
status:number;
weight:number;
is_can_comment:number;
created_at:Date;
updated_at:Date;
deleted_at:Date;
creator:string;
category_id:string;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  cmsArticless: CmsArticles[];
  total: number;
}