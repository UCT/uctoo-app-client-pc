export interface MinishopActivityGrouponLog {
[property: string]: any;
id:string;
user_id:string;
user_nickname:string;
user_avatar:string;
groupon_id:string;
goods_id:string;
goods_sku_price_id:string;
activity_id:string;
is_leader:number;
is_fictitious:number;
order_id:string;
is_refund:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopActivityGrouponLogs: MinishopActivityGrouponLog[];
  total: number;
}