export interface MinishopDispatchExpress {
[property: string]: any;
id:string;
type:string;
weigh:number;
first_num:number;
first_price:string;
additional_num:number;
additional_price:string;
province_ids:string;
city_ids:string;
area_ids:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopDispatchExpresss: MinishopDispatchExpress[];
  total: number;
}