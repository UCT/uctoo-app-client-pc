export interface MinishopDispatchAutosend {
[property: string]: any;
id:string;
type:string;
content:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopDispatchAutosends: MinishopDispatchAutosend[];
  total: number;
}