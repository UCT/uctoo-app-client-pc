export interface LinkGroup {
[property: string]: any;
id:string;
owner:string;
groupname:string;
name:string;
description:string;
picture:string;
stars:string;
watcher_count:number;
linked_count:number;
links_count:number;
privacy_level:number;
created_at:Date;
creator:string;
deleted_at:Date;
updated_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  linkGroups: LinkGroup[];
  total: number;
}