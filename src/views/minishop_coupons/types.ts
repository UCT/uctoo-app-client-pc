export interface MinishopCoupons {
[property: string]: any;
id:string;
name:string;
type:string;
goods_ids:string;
amount:string;
enough:string;
stock:number;
limit:number;
get_time:Date;
use_time:Date;
description:string;
use_time_start:Date;
use_time_end:Date;
get_time_start:Date;
get_time_end:Date;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopCouponss: MinishopCoupons[];
  total: number;
}