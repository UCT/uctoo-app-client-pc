export interface MinishopActivity {
[property: string]: any;
id:string;
title:string;
goods_ids:string;
type:string;
richtext_title:string;
sharde_time:Date;
end_time:Date;
rules:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
richtext_id:string;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopActivitys: MinishopActivity[];
  total: number;
}