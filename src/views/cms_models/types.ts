export interface CmsModels {
[property: string]: any;
id:string;
name:string;
alias:string;
table_name:string;
description:string;
used_at_detail:string;
used_at_search:string;
used_at_list:string;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  cmsModelss: CmsModels[];
  total: number;
}