export interface CmsTags {
[property: string]: any;
id:string;
name:string;
title:string;
keywords:string;
description:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  cmsTagss: CmsTags[];
  total: number;
}