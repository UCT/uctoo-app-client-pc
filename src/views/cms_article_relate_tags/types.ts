export interface CmsArticleRelateTags {
[property: string]: any;
id:string;
article_id:string;
tag_id:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
creator:string;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  cmsArticleRelateTagss: CmsArticleRelateTags[];
  total: number;
}