export interface MinishopAddressInfo {
[property: string]: any;
id:string;
receiver_name:string;
detailed_address:string;
mobile:string;
country:string;
province:string;
city:string;
town:string;
default:number;
status:number;
creator:string;
created_at:Date;
updated_at:Date;
deleted_at:Date;
}
export interface Response {
  [property: string]: any;
  currentPage: number;
  minishopAddressInfos: MinishopAddressInfo[];
  total: number;
}